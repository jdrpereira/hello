module gitlab.com/jdrpereira/hello

go 1.14

require (
	github.com/Pallinder/go-randomdata v1.2.0 // indirect
	gitlab.com/jdrpereira/randgen v0.0.0-20200325142917-28308f8ad296
	golang.org/x/text v0.3.2 // indirect
)
